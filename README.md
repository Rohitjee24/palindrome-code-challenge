# README #


### This Repo is For Palindrome coding challenge? ###

* Palindrome challenge
* 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Objectives to be completed ###

*OBJECTIVE 1: STRING NORMALIZATION

Implement a String function cleanString(String str) which removes all nonalphabetic
characters from a string and converts all characters to upper case.


*OBJECTIVE 2: PALINDROME FUNCTION

Implement the Boolean function isPalindrome (String word) to check if a word
is a palindrome. Your function should not consider non-alphabetic characters,
and it should not be case-sensitive, e.g. isPalindrome(“nO@^8oN”) should
return true.


*OBJECTIVE 3: SORTING

Implement a function sortStrings (String[ ] words) that will sort a large array of
strings in alphabetical order. Please ensure to keep scalability in mind when
designing this method.


### Performance Analysis Report of that Program? ###

Yes, Performance Analysis report is ready but i don't want to share it in the repo.
My program had been tested with 99900 words (data-set) and it has a record of completing the task in half a second. which is high performance in Less time.