package org.rohit;

import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Created by rohit on 3/1/2017.
 */
public class StringManipulatorTests {

    private String[] firstArray = {"Rohit", "Allen", "Block", "Denver"};
    private String[] secondArray = {"Waheed", "Rakesh", "Raju", "Alla"};
    private String[] thirdArray = {"Debi", "Allen", "Mona", "Doller"};

    private String[] firstArraySorted = { "Allen", "Block", "Denver", "Rohit"};
    private String[] secondArraySorted = { "Alla", "Raju","Rakesh","Waheed"};
    private String[] thirdArraySorted = { "Allen", "Debi","Doller", "Mona"};


    @Test
    public void testSortingMethod() throws Exception{
        assertEquals(StringManipulator.sortStrings(firstArray),firstArraySorted );
        assertEquals(StringManipulator.sortStrings(secondArray),secondArraySorted );
        assertEquals(StringManipulator.sortStrings(thirdArray),thirdArraySorted );

    }


    @Test
    public void testCleanString(){
        assertEquals(StringManipulator.cleanString("nO@^8oN"), "NOON");
        assertEquals(StringManipulator.cleanString("asdFjDK"), "ASDFJDK");
        assertEquals(StringManipulator.cleanString("tAxi"), "TAXI");
        assertEquals(StringManipulator.cleanString("Trust"), "TRUST");
        assertEquals(StringManipulator.cleanString("rAInForEst"), "RAINFOREST");
        assertEquals(StringManipulator.cleanString("asdf#&ASDFf@#&dsaFDSA"), "ASDFASDFFDSAFDSA");
        assertEquals(StringManipulator.cleanString("asdF&_!5asdFdsA#fdsA"), "ASDFASDFDSAFDSA");
        assertEquals(StringManipulator.cleanString("pickle"), "PICKLE");
        assertEquals(StringManipulator.cleanString("computer"), "COMPUTER");

    }


    @Test
    public void testisPalindrome(){
        assertTrue(StringManipulator.isPalindrome(StringManipulator.cleanString("nO@^8oN")));
        assertFalse(StringManipulator.isPalindrome(StringManipulator.cleanString("Trust")));
        assertTrue(StringManipulator.isPalindrome(StringManipulator.cleanString("asdf#&ASDFf@#&dsaFDSA")));
        assertTrue(StringManipulator.isPalindrome(StringManipulator.cleanString("asdF&_!5asdFdsA#fdsA")));
    }


}
