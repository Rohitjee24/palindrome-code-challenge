package org.rohit;

import java.io.*;
import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by rohit on 3/1/2017.
 */
public class StringManipulator {

    // Remove non-alphabetic characters using Regex.
    private static final String REGEX = "[^a-zA-Z]";

    // Objective 1
    public static String cleanString(String str){
        str = str.replaceAll(REGEX, "");
        return str.toUpperCase();
    }

    // Objective 2
    public static Boolean  isPalindrome(String word){
        String reverse;
        if(word != null|| !word.isEmpty()) {
            word = cleanString(word);
            reverse = new StringBuilder(word).reverse().toString();
            if(word.equalsIgnoreCase(reverse)){
                return true;
            }
        }

        return false;
    }

    // Objective 3
    public static String [] sortStrings(String[] words){
        //Arrays.sort(words); // Sort an Array
        Arrays.parallelSort(words); // for huge arrays sorting
        return words;
    }


    public static void main(String[] args) throws IOException {
        String word;

        Map<String, String> wordMap = new TreeMap<String, String>();

        // Creating Buffer reader and Buffer writer for reading and Writing a file from a certain location.
        BufferedReader br = new BufferedReader(new FileReader("src/atdd/palindrome_tests.csv"));
        BufferedWriter bw = new BufferedWriter(new FileWriter("src/atdd/palindrome_sorted.csv"));;

        br.readLine(); // won't read the first row

        while ((word = br.readLine()) != null) {
            word = cleanString(word);
            wordMap.put(word, isPalindrome(word).toString());
        }

        br.close();// Closing BufferReader Object

        // Print the output to a file.
        bw.write("Words\t\tPalindrome  ");
        bw.newLine();
        // Iterating through a Map and writing it into a  File
        for (Map.Entry<String, String> entry : wordMap.entrySet()) {
            bw.write(entry.getKey() + "\t\t" + entry.getValue());
            bw.newLine();
        }

        bw.close(); // Closing BufferWriter Object

    }

}
